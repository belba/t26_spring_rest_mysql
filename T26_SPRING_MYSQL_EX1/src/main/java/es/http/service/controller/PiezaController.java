package es.http.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.http.service.dto.Pieza;
import es.http.service.service.IPiezaService;

@RestController
@RequestMapping("/api")
public class PiezaController {

	@Autowired
	IPiezaService piezaService;
	
	@GetMapping("/piezas")
	public List<Pieza> getAllPiezas(){
		return piezaService.getAllPiezas();
	}
	
	@GetMapping("/pieza/{id}")
	public Pieza getPieza(@PathVariable(name="id") Long id) {
		Pieza p = new Pieza();
		p = piezaService.getPieza(id);
		return p;
	}
	
	@PostMapping("/pieza")
	public Pieza savePieza(@RequestBody Pieza pieza) {
		return piezaService.savePieza(pieza);
	}
	
	@PutMapping("/pieza/{id}")
	public Pieza updatePieza(@PathVariable(name="id") Long id, @RequestBody Pieza pieza) {
		Pieza p_select = new Pieza();
		Pieza p_updated = new Pieza();
		
		p_select = piezaService.getPieza(id);
		
		p_select.setCodigo(pieza.getCodigo());
		p_select.setNombre(pieza.getNombre());
		p_select.setSumnistra(pieza.getSumnistra());
		
		p_updated = piezaService.updatePieza(p_select);
		
		return p_updated;
	}
	
	@DeleteMapping("/pieza/{id}")
	public void deletePieza(@PathVariable(name="id") Long id) {
		piezaService.deletePieza(id);
	}
}
