package es.http.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.http.service.dto.Suministra;
import es.http.service.service.ISuministraService;

@RestController
@RequestMapping("/api")
public class SuministraController {

	@Autowired
	ISuministraService suministraService;
	
	@GetMapping("/suministros")
	public List<Suministra> getAllSuministros(){
		return suministraService.getAllSuministros();
	}
	
	@GetMapping("/suministra/{id}")
	public Suministra getSuministra(@PathVariable(name="id") Long id) {
		Suministra sum = new Suministra();
		sum = suministraService.getSuministra(id);
		
		return sum;
	}
	
	@PostMapping("/suministra")
	public Suministra saveSuministra(@RequestBody Suministra suministra) {
		return suministraService.saveSuministra(suministra);
	}
	
	@PutMapping("/suministra/{id}")
	public Suministra updateSuministra(@PathVariable(name="id") Long id, @RequestBody Suministra suministra) {
		Suministra sum_selected = new Suministra();
		Suministra sum_updated = new Suministra();
		
		sum_selected = suministraService.getSuministra(id);
		
		sum_selected.setId(suministra.getId());
		sum_selected.setNombre(suministra.getNombre());
		sum_selected.setPiezas(suministra.getPiezas());
		sum_selected.setProveedor(suministra.getProveedor());
	
		sum_selected = suministraService.updatedSuministra(sum_selected);
		return sum_updated;
	}
	
	@DeleteMapping("/suministra/{id}")
	public void DeleteSuministra(@PathVariable(name="id") Long id) {
		suministraService.deleteSuminstra(id);
	}
}
