package es.http.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.http.service.dto.Proveedor;
import es.http.service.service.IProveedorService;

@RestController
@RequestMapping("/api")
public class ProveedorController {

	@Autowired
	IProveedorService proveedorService;
	
	@GetMapping("/proveedores")
	public List<Proveedor> getAllProveedores() {
		return proveedorService.getAllProveedor();
	}
	
	@GetMapping("/proveedor/{id}")
	public Proveedor getProveedor(@PathVariable(name="id") String id) {
		Proveedor prov = new Proveedor();
		prov = proveedorService.getProveedor(id);
		
		return prov;
		
	}
	
	@PostMapping("/proveedor")
	public Proveedor saveProveedor(@RequestBody Proveedor proveedor) {
		return proveedorService.saveProveedor(proveedor);
	}
	
	@PutMapping("proveedor/{id}")
	public Proveedor updatePieza(@PathVariable(name="id") String id, @RequestBody Proveedor proveedor) {
		Proveedor pro_selected = new Proveedor();
		Proveedor pro_updated = new Proveedor();
		
		pro_selected = proveedorService.getProveedor(id);
		
		pro_selected.setId(proveedor.getId());
		pro_selected.setNombre(proveedor.getNombre());
		pro_selected.setSumnistra(proveedor.getSumnistra());
		
		pro_updated = proveedorService.updateProveedor(pro_selected);
		
		return pro_updated;	
	}
	
	@DeleteMapping("/proveedor/{id}")
	public void deleteProveedor(@PathVariable(name="id") String id) {
		proveedorService.deleteProveedor(id);
	}
	
	
}
