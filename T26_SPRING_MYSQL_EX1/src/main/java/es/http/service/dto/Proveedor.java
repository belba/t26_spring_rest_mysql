package es.http.service.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name="proveedores")
public class Proveedor {
	
	@Id
	@Column(name = "id", length=4)
	@NotNull
	private String id;
	@Column(name = "nombre", length=100)
	private String nombre;
	
	@OneToMany
    @JoinColumn(name="id")
    private List<Suministra> sumnistra;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public List<Suministra> getSumnistra() {
		return sumnistra;
	}

	public void setSumnistra(List<Suministra> sumnistra) {
		this.sumnistra = sumnistra;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	} 

	
	
}
