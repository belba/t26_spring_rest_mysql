package es.http.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.http.service.dao.IProveedorDao;
import es.http.service.dto.Proveedor;

@Service
public class ProveedorServiceImpl implements IProveedorService{
	
	@Autowired
	private IProveedorDao iProveedorDao;

	@Override
	public List<Proveedor> getAllProveedor() {
		return iProveedorDao.findAll();
	}

	@Override
	public Proveedor getProveedor(String id) {
		return iProveedorDao.findById(id).get();
	}

	@Override
	public Proveedor saveProveedor(Proveedor proveedor) {
		return iProveedorDao.save(proveedor);
	}

	@Override
	public Proveedor updateProveedor(Proveedor proveedor) {
		return iProveedorDao.save(proveedor);
	}

	@Override
	public void deleteProveedor(String id) {
		iProveedorDao.deleteById(id);
	}
	
	
	
}
