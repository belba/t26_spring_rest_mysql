package es.http.service.service;

import java.util.List;

import es.http.service.dto.Pieza;

public interface IPiezaService {
	
	//Metodo del CRUD
	public List<Pieza> getAllPiezas();
	
	public Pieza getPieza(Long id);
	
	public Pieza savePieza(Pieza pieza);

	public Pieza updatePieza(Pieza pieza);
	
	public void deletePieza(Long id);
}
