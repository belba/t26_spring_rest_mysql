package es.http.service.service;

import java.util.List;

import es.http.service.dto.Suministra;

public interface ISuministraService {

	//Metodos CRUD
	public List<Suministra> getAllSuministros();

	public Suministra getSuministra(Long id);

	public Suministra saveSuministra(Suministra suministra);
	
	public Suministra updatedSuministra(Suministra suministra);
	
	public void deleteSuminstra(Long id);

}
