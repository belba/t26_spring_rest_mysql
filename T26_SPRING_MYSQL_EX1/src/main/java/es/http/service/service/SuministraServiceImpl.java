package es.http.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.http.service.dao.ISuministraDao;
import es.http.service.dto.Suministra;

@Service
public class SuministraServiceImpl implements ISuministraService{

	@Autowired
	ISuministraDao suministraDao;
	
	@Override
	public List<Suministra> getAllSuministros() {
		return suministraDao.findAll();
	}

	@Override
	public Suministra getSuministra(Long id) {
		return suministraDao.findById(id).get();
	}

	@Override
	public Suministra saveSuministra(Suministra suministra) {
		return suministraDao.save(suministra);
	}

	@Override
	public Suministra updatedSuministra(Suministra suministra) {
		return suministraDao.save(suministra);
	}

	@Override
	public void deleteSuminstra(Long id) {
		suministraDao.deleteById(id);
	}

}
