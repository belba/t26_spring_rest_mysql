package es.http.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.http.service.dao.IPiezaDao;
import es.http.service.dto.Pieza;

@Service
public class PiezaServiceImpl implements IPiezaService {

	@Autowired
	IPiezaDao iPiezaDao;
	
	@Override
	public List<Pieza> getAllPiezas() {
		return iPiezaDao.findAll();
	}

	@Override
	public Pieza getPieza(Long id) {
		return iPiezaDao.findById(id).get();
	}

	@Override
	public Pieza savePieza(Pieza pieza) {
		return iPiezaDao.save(pieza);
	}

	@Override
	public Pieza updatePieza(Pieza pieza) {
		return iPiezaDao.save(pieza);
	}

	@Override
	public void deletePieza(Long id) {
		iPiezaDao.deleteById(id);
	}

}
