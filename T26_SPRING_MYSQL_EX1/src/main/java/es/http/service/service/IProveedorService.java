package es.http.service.service;

import java.util.List;

import es.http.service.dto.Proveedor;

public interface IProveedorService {

	//Metodos CRUD
	
	public List<Proveedor> getAllProveedor();

	public Proveedor getProveedor(String id);

	public Proveedor saveProveedor(Proveedor proveedor);
	
	public Proveedor updateProveedor(Proveedor proveedor);
	
	public void deleteProveedor(String id);
}
