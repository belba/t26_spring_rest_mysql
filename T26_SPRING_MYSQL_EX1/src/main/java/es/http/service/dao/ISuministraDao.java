package es.http.service.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import es.http.service.dto.Suministra;

public interface ISuministraDao extends JpaRepository<Suministra, Long>{

}
