INSERT INTO facultades VALUES (1,'Oxford');
INSERT INTO facultades VALUES (2,'Oxford 2');
INSERT INTO facultades VALUES (3,'Oxford 3');

INSERT INTO investigadores VALUES ("6666988E",'Juan Maria',1);
INSERT INTO investigadores VALUES ("9999998D",'Maria Perez',2);
INSERT INTO investigadores VALUES ("9898888D",'Mohamed Ali',2);

INSERT INTO equipos VALUES ("E121",'Equipo 1', 1);
INSERT INTO equipos VALUES ("B223",'Equipo 2', 2);
INSERT INTO equipos VALUES ("C343",'Equipo 3', 2);

INSERT INTO reservas VALUES (1,'E121', '6666988E');
INSERT INTO reservas VALUES (2,'E121', '9999998D');