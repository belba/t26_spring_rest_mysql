package es.http.service.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name="equipos")
public class Equipo {
	
	@Id
	@Column(name="numSerie", length=4)
	@NotNull
	private String numSerie;
	
	@Column(name="nombre", length=255)
	private String nombre;
	
	@ManyToOne
    @JoinColumn(name = "codigo_fac")
    Facultad facultad;
	
	@OneToMany
    @JoinColumn(name = "id")
	private List<Reserva> reserva;

	public String getNumSerie() {
		return numSerie;
	}

	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Facultad getFacultad() {
		return facultad;
	}

	public void setFacultad(Facultad facultad) {
		this.facultad = facultad;
	}

	public List<Reserva> getReserva() {
		return reserva;
	}

	public void setReserva(List<Reserva> reserva) {
		this.reserva = reserva;
	}

	

	
}
