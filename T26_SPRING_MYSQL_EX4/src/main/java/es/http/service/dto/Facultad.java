package es.http.service.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name="facultades")
public class Facultad {

	@Id
	@Column(name="codigo")
	@NotNull
	private int codigo;

	@Column(name="nombre", length=100)
	private String nombre;

	@OneToMany
    @JoinColumn(name="dni")
    private List<Investigador> investigadors;
	
	@OneToMany
    @JoinColumn(name="numSerie")
    private List<Equipo> equipos;
	
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Investigador> getInvestigadors() {
		return investigadors;
	}

	public void setInvestigadors(List<Investigador> investigadors) {
		this.investigadors = investigadors;
	}

	public List<Equipo> getEquipos() {
		return equipos;
	}

	public void setEquipos(List<Equipo> equipos) {
		this.equipos = equipos;
	}

	
	
	
}
