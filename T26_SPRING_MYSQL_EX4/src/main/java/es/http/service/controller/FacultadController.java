package es.http.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.http.service.dto.Facultad;
import es.http.service.service.IFacultadService;

@RestController
@RequestMapping("/api")
public class FacultadController {

	@Autowired
	IFacultadService facultadService;
	
	@GetMapping("/facultades")
	public List<Facultad> getAllFacultades() {
		return facultadService.getAllFacultades();
	}
	
	@GetMapping("/facultad/{id}")
	public Facultad getFacultad(@PathVariable(name="id") int codigo) {
		Facultad fac = new Facultad();
		fac = facultadService.getFacultad(codigo);
		return fac;
	}
	
	@PostMapping("/facultad")
	public Facultad saveFacultad(@RequestBody Facultad facultad) {
		return facultadService.saveFacultad(facultad);
	}
	
	@PutMapping("/facutlad/{id}")
	public Facultad updateFacultad(@PathVariable(name="id") int codigo,@RequestBody Facultad facultad) {
		Facultad fac_selected = new Facultad();
		Facultad fac_updated = new Facultad();
		
		fac_selected = facultadService.getFacultad(codigo);

		fac_selected.setCodigo(facultad.getCodigo());
		fac_selected.setEquipos(facultad.getEquipos());
		fac_selected.setInvestigadors(facultad.getInvestigadors());
		fac_selected.setNombre(facultad.getNombre());
		
		fac_updated = facultadService.updateFacultad(fac_selected);
		
		return fac_updated;
	}
	
	@DeleteMapping("/facultad/{id}")
	public void deleteFacultad(@PathVariable(name="id") int codigo) {
		facultadService.deleteFacultad(codigo);
	}
}
