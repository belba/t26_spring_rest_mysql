package es.http.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.http.service.dto.Investigador;
import es.http.service.service.IInvestigadorService;

@RestController
@RequestMapping("/api")
public class InvestigadorController {

	@Autowired
	IInvestigadorService investigadorService;
	
	@GetMapping("/investigadores")
	public List<Investigador> getAllEquipos(){
		return investigadorService.getAllInvestigadores();
	}
	
	@GetMapping("/investigador/{id}")
	public Investigador getInvestigador(@PathVariable(name="id") String dni) {
		Investigador inv =new Investigador();
		inv = investigadorService.getInvestigador(dni);
		return inv;
	}
	
	@PostMapping("/investigador")
	public Investigador saveEquipo(@RequestBody Investigador investigador) {
		return investigadorService.saveInvestigador(investigador);
	}

	@PutMapping("/investigador/{id}")
	public Investigador uptadeEquipo(@PathVariable(name="dni") String dni, @RequestBody Investigador investigador) {
		Investigador inv_selected = new Investigador();
		Investigador inv_updated = new Investigador();
		
		inv_selected.setDni(investigador.getDni());
		inv_selected.setNomApels(investigador.getNomApels());
		inv_selected.setFacultad(investigador.getFacultad());
		inv_selected.setReserva(investigador.getReserva());

		inv_updated = investigadorService.saveInvestigador(inv_selected);
		
		return inv_updated;
	}
	
	@DeleteMapping("/investigador/{id}")
	public void deleteInvestigador(@PathVariable(name="id") String dni) {
		investigadorService.deleteInvestigador(dni);
	}


}

