package es.http.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.http.service.dto.Equipo;
import es.http.service.service.IEquipoService;

@RestController
@RequestMapping("/api")
public class EquipoController {

	@Autowired
	IEquipoService equipoService;
	
	@GetMapping("/equipos")
	public List<Equipo> getAllEquipos(){
		return equipoService.getAllEquipos();
	}
	
	@GetMapping("/equipo/{id}")
	public Equipo getEquipo(@PathVariable(name="id") String numSerie) {
		Equipo eq =new Equipo();
		eq = equipoService.getEquipo(numSerie);
		return eq;
	}
	
	@PostMapping("/equipo/")
	public Equipo saveEquipo(@RequestBody Equipo equipo) {
		return equipoService.saveEquipo(equipo);
	}
	
	@PutMapping("/equipo/{id}")
	public Equipo updateEquipo(@PathVariable(name="id") String numSerie, @RequestBody Equipo equipo) {
		Equipo eq_selected = new Equipo();
		Equipo eq_updated = new Equipo();
		
		eq_selected.setFacultad(equipo.getFacultad());
		eq_selected.setNombre(equipo.getNombre());
		eq_selected.setNumSerie(equipo.getNumSerie());
		eq_selected.setReserva(equipo.getReserva());
		
		eq_updated = equipoService.saveEquipo(eq_selected);
		
		return eq_updated;
	}
	
	@DeleteMapping("/equipo/{id}")
	public void deleteEquipo(@PathVariable(name="id") String numSerie) {
		equipoService.deleteEquipo(numSerie);
	}
}
