package es.http.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.http.service.dto.Reserva;
import es.http.service.service.IReservaService;

@RestController
@RequestMapping("/api")
public class ReservaController {
	
	@Autowired
	IReservaService reservaService;
	
	@GetMapping("/reservas")
	public List<Reserva> getAllReservas(){
		return reservaService.getAllReservas();
	}
	
	@GetMapping("/reserva/{id}")
	public Reserva getReserva(@PathVariable(name="id") Long id) {
		Reserva res = new Reserva();
		res = reservaService.getReserva(id);
		return res;
	}
	
	@PostMapping("/reserva")
	public Reserva saveReserva(@RequestBody Reserva reserva) {
		return reservaService.saveReserva(reserva);
	}
	
	@PutMapping("/reserva/{id}")
	public Reserva updateReserva(@PathVariable(name="id") Long id, @RequestBody Reserva reserva) {
		Reserva res_selected = new Reserva();
		Reserva res_updated = new Reserva();

		res_selected.setId(reserva.getId());
		res_selected.setEquipo(reserva.getEquipo());
		res_selected.setInvestigador(reserva.getInvestigador());

		res_updated = reservaService.saveReserva(res_selected);
		
		return res_updated;
	}
	
	@DeleteMapping("/reserva/{id}")
	public void deleteReserva(@PathVariable(name="id") Long id) {
		reservaService.deleteReserva(id);
	}
}
