package es.http.service.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import es.http.service.dto.Equipo;

public interface IEquipoDao extends JpaRepository<Equipo, String>{

}
