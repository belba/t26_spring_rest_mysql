package es.http.service.service;

import java.util.List;

import es.http.service.dto.Investigador;

public interface IInvestigadorService {

	//Metodo del CRUD
	public List<Investigador> getAllInvestigadores();
	
	public Investigador getInvestigador(String dni);

	public Investigador saveInvestigador(Investigador investigador);
	
	public Investigador updateInvestigador(Investigador investigador);
	
	public void deleteInvestigador(String dni);
}

