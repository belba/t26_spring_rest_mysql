package es.http.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.http.service.dao.IReservaDao;
import es.http.service.dto.Reserva;

@Service
public class ReservaServiceImpl implements IReservaService{

	@Autowired
	IReservaDao reservaDao;

	@Override
	public List<Reserva> getAllReservas() {
		// TODO Auto-generated method stub
		return reservaDao.findAll();
	}

	@Override
	public Reserva getReserva(Long id) {
		// TODO Auto-generated method stub
		return reservaDao.findById(id).get();
	}

	@Override
	public Reserva saveReserva(Reserva reserva) {
		// TODO Auto-generated method stub
		return reservaDao.save(reserva);
	}

	@Override
	public Reserva updateReserva(Reserva reserva) {
		// TODO Auto-generated method stub
		return reservaDao.save(reserva);
	}

	@Override
	public void deleteReserva(Long id) {
		// TODO Auto-generated method stub
		reservaDao.deleteById(id);
	}
	
	
	
}
