package es.http.service.service;

import java.util.List;

import es.http.service.dto.Reserva;

public interface IReservaService {

	//Metodo del CRUD
	public List<Reserva> getAllReservas();
	
	public Reserva getReserva(Long id);

	public Reserva saveReserva(Reserva reserva);
	
	public Reserva updateReserva(Reserva reserva);
	
	public void deleteReserva(Long id);
	
}
