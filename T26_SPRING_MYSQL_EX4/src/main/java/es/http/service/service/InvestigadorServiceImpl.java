package es.http.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.http.service.dao.IInvestigadorDao;
import es.http.service.dto.Investigador;

@Service
public class InvestigadorServiceImpl implements IInvestigadorService{

	@Autowired
	IInvestigadorDao investigadorDao;
	
	@Override
	public List<Investigador> getAllInvestigadores() {
		// TODO Auto-generated method stub
		return investigadorDao.findAll();
	}

	@Override
	public Investigador getInvestigador(String dni) {
		// TODO Auto-generated method stub
		return investigadorDao.findById(dni).get();
	}

	@Override
	public Investigador saveInvestigador(Investigador investigador) {
		// TODO Auto-generated method stub
		return investigadorDao.save(investigador);
	}

	@Override
	public Investigador updateInvestigador(Investigador investigador) {
		// TODO Auto-generated method stub
		return investigadorDao.save(investigador);
	}

	@Override
	public void deleteInvestigador(String dni) {
		// TODO Auto-generated method stub
		investigadorDao.deleteById(dni);
	}

}
