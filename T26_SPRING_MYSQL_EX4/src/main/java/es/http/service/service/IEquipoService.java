package es.http.service.service;

import java.util.List;

import es.http.service.dto.Equipo;

public interface IEquipoService {

	//Metodo del CRUD
	public List<Equipo> getAllEquipos();
	
	public Equipo getEquipo(String numSerie);

	public Equipo saveEquipo(Equipo equipo);
	
	public Equipo updateEquipo(Equipo equipo);
	
	public void deleteEquipo(String numSerie);
}
