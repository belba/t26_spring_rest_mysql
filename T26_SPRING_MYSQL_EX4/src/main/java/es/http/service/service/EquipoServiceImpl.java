package es.http.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.http.service.dao.IEquipoDao;
import es.http.service.dto.Equipo;

@Service
public class EquipoServiceImpl implements IEquipoService{

	@Autowired
	IEquipoDao equipoDao;

	@Override
	public List<Equipo> getAllEquipos() {
		// TODO Auto-generated method stub
		return equipoDao.findAll();
	}

	@Override
	public Equipo getEquipo(String numSerie) {
		// TODO Auto-generated method stub
		return equipoDao.findById(numSerie).get();
	}

	@Override
	public Equipo saveEquipo(Equipo equipo) {
		// TODO Auto-generated method stub
		return equipoDao.save(equipo);
	}

	@Override
	public Equipo updateEquipo(Equipo equipo) {
		// TODO Auto-generated method stub
		return equipoDao.save(equipo);
	}

	@Override
	public void deleteEquipo(String numSerie) {
		// TODO Auto-generated method stub
		equipoDao.deleteById(numSerie);
	}
	
	
}
