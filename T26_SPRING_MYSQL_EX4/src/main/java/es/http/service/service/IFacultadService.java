package es.http.service.service;

import java.util.List;

import es.http.service.dto.Facultad;

public interface IFacultadService {

	//Metodo del CRUD
	public List<Facultad> getAllFacultades();
	
	public Facultad getFacultad(int codigo);
	
	public Facultad saveFacultad(Facultad facultad);
	
	public Facultad updateFacultad(Facultad facultad);
	
	public void deleteFacultad(int codigo);

}
