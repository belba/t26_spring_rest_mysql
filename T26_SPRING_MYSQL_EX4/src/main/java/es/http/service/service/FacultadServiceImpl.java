package es.http.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.http.service.dao.IFacultadDao;
import es.http.service.dto.Facultad;

@Service
public class FacultadServiceImpl implements IFacultadService{

	@Autowired
	IFacultadDao facultadDao;

	@Override
	public List<Facultad> getAllFacultades() {
		// TODO Auto-generated method stub
		return facultadDao.findAll();
	}

	@Override
	public Facultad getFacultad(int codigo) {
		// TODO Auto-generated method stub
		return facultadDao.findById(codigo).get();
	}

	@Override
	public Facultad saveFacultad(Facultad facultad) {
		// TODO Auto-generated method stub
		return facultadDao.save(facultad);
	}

	@Override
	public Facultad updateFacultad(Facultad facultad) {
		// TODO Auto-generated method stub
		return facultadDao.save(facultad);
	}

	@Override
	public void deleteFacultad(int codigo) {
		// TODO Auto-generated method stub
		facultadDao.deleteById(codigo);
	}
}
